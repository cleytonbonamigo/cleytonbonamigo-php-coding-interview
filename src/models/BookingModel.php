<?php

namespace Src\models;

class BookingModel {

	private $bookingData;
    private string $fileName;

	function __construct() {
        $this->fileName = dirname(__DIR__) . '/../scripts/bookings.json';
		$string = file_get_contents($this->fileName);
		$this->bookingData = json_decode($string, true);
	}

	public function getBookings() {
		return $this->bookingData;
	}

    public function bookANewClient(array $newBook): array
    {
        $bookingData = $this->getBookings();
        $bookingData[] = $newBook;

        file_put_contents($this->fileName, json_encode($bookingData));

        return $newBook;
    }
}